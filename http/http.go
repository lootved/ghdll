package http

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func Download(repos string, releasePath string, regex string, extension string) {
	splited := strings.Split(repos, "/")
	if len(splited) != 2 {
		log.Fatalln("invalid repos parameter, must be in the form `username/project`")
	}
	url := GetLatestUrl(repos, releasePath, regex, extension)
	download(url, strings.ToLower(splited[1])+extension)
}

func GetLatestUrl(repos string, releasePath string, urlRegex string, extension string) string {
	const baseUrl = "https://github.com"

	if extension[0] != '.' {
		log.Fatalln("extension must start with .")
	}

	regex := fmt.Sprintf(`href="(.*/%s.*%s)`, urlRegex, strings.ReplaceAll(extension, ".", `\.`))
	re := regexp.MustCompile(regex)

	url := baseUrl + "/" + repos + releasePath
	log.Println("Fetching ", url)
	log.Println("Looking for regex:", regex)

	resp, err := http.Get(url)
	check(err)
	defer resp.Body.Close()

	sc := bufio.NewScanner(resp.Body)
	for sc.Scan() {
		line := sc.Text()
		res := re.FindAllStringSubmatch(line, 1)
		if len(res) > 0 {
			return baseUrl + res[0][1]
		}
	}
	log.Fatalln("unable to parse url from release page")
	return ""
}

func download(url string, dest string) {

	start := time.Now()
	out, err := os.Create(dest)
	check(err)
	defer out.Close()

	headResp, err := http.Head(url)
	check(err)
	defer headResp.Body.Close()

	resp, err := http.Get(url)
	check(err)
	size, err := strconv.Atoi(headResp.Header.Get("Content-Length"))
	check(err)

	defer resp.Body.Close()

	chunkSize := size / 10
	log.Println("Downloading file to ", dest)
	copyByChunk(resp.Body, out, chunkSize, size, displayPercent)

	elapsed := time.Since(start)
	fmt.Fprintln(os.Stderr, "")
	log.Printf("Download took %s", elapsed)
}

func copyByChunk(r io.Reader, w io.Writer,
	chunkSize int, expectedSize int,
	cb func(int)) {
	var buf = make([]byte, chunkSize)
	var processed int
	for {
		n, err := r.Read(buf)
		if err == io.EOF {
			break
		}
		processed = processed + 100*n
		cb(processed / expectedSize)
		w.Write(buf[:n])
	}
}

func displayPercent(percentage int) {
	symboldNbr := 20
	equalNbr := percentage / 5
	line := "[ " + strings.Repeat("=", equalNbr) + ">" +
		strings.Repeat(" ", symboldNbr-equalNbr) + "]"

	fmt.Fprintf(os.Stderr, "\r%s %d", line, percentage)
	fmt.Fprint(os.Stderr, "%")
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
