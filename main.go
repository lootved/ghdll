package main

import (
	"flag"
	"fmt"

	"gitlab.com/lootved/ghdll/http"
)

func main() {

	var repos string
	var regex string
	var extension string

	var help bool
	var dll bool
	var useTag bool
	flag.BoolVar(&help, "h", false, "display usage")
	flag.BoolVar(&useTag, "tag", false, "use /releases as suffix instead of /releases/latest")
	flag.BoolVar(&dll, "dll", false, "flag to download")

	flag.StringVar(&repos, "repos", "VSCodium/vscodium", "repos to download artifact from")
	flag.StringVar(&regex, "regex", "VSCodium-linux-x64", "regex used to extract the url from the release page")
	flag.StringVar(&extension, "extension", ".tar.gz", "extension used to extract the url from the release page")

	flag.Parse()

	releasePath := "/releases/latest"

	if help {
		flag.PrintDefaults()
		return
	}
	if useTag {
		releasePath = "/releases"
	}
	if dll {
		http.Download(repos, releasePath, regex, extension)
	} else {
		url := http.GetLatestUrl(repos, releasePath, regex, extension)
		fmt.Println(url)
	}

}
